#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>

#include "client.h"

#define LISTEN_QUEUE 20

using namespace std;

int client::child_proc()
{
    int sockfd, n;
    struct sockaddr_in servAddr;
    //struct hostent *server;

    char buffer[1024];

    fprintf(stdout, "Child PID %d: Got Server's Port %d\n", getpid(), mgr_port);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        die("Unable to create client socket\n");

    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    servAddr.sin_port =  htons(mgr_port);

    if (connect (sockfd, (struct sockaddr * ) &servAddr, sizeof(servAddr) ) < 0) 
        die ("Unable to connect to server\n");
    
    fprintf(stdout, "Client %d connected\n", getpid());    
    memset(buffer, 0, 1023);
    n = read(sockfd, buffer, 1023);
    if (n < 0) 
        die ("Unable to read data from server\n");

    // fprintf(stdout, "Client %d received: %s\n", getpid(), buffer);
    
    //nonce = 0;
    //stripLastNewline(buffer);

    populate_self(buffer); // initialize all members

    // set udp server
	if (open_udp_server_sock() < 0)
		die("Client could not set up udp_server!\n");

    // am I the first client?
    if (first_client == true) {
    	suc_id = hashval;
		pre_id = hashval;
		suc_port = my_udp_port;
		pre_port = my_udp_port;
		fc_id = hashval;
		// no updates involved here

    } else {
        //I know about first client now - need to find my successor and predecessor
    	fc_id = nonce_name_hash(nonce, fc_name);
    	
        if (get_stage() >= 4)
            establish_ring_with_pred_succ_stage4(); // no pred.succ updates
        else
            establish_ring_with_pred_succ();

    }

    if (get_stage() >= 4) {
        setup_finger_tables();
    }

    // respond to manager
    memset(buffer, 0, 1023);
    sprintf(buffer, "%u\n%u\n", nonce+(uint32_t)pid, my_udp_port); 
    // fprintf(stdout, "Client sending: %s\n", buffer);

    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0)
        die ("Unable to read data from server\n");

    set_mgr_sock(sockfd);
    //fprintf(stdout, "Client: Setting Manager socket as: %d\n", );

    fprintf(fptr, "client %s created with hash 0x%x\n", name, hashval);

    select_loop();

    fprintf(stdout, "Client: %s exiting\n", name);
    return 0;
}

void client::start()
{
    fprintf(stdout, "In client::start\n");
    child_proc();
}

int client::open_udp_server_sock()
{
    struct sockaddr_in servAddr;
    socklen_t len;
    int err = 0;
    int listen_sock = 0;
    
    len = sizeof(servAddr);
    memset(&servAddr, 0, sizeof(servAddr));
    
    if ( (listen_sock = socket(AF_INET, SOCK_DGRAM, 0) ) < 0 )
        die ("Unable to create socket\n");

    set_listen_sock(listen_sock);

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port = htons(0);

    if( ( err = bind(listen_sock, (struct sockaddr *) &servAddr, sizeof(servAddr))<0 ) ) 
        die ("Problem in bind\n");
    
    /*if ( (err = listen(listen_sock, LISTEN_QUEUE) ) < 0)
        die ("Problem in listen\n");*/

    if (getsockname(listen_sock, (struct sockaddr *)&servAddr, &len) < 0)
        die("Getsockname failed\n");
    else {
        fprintf(stdout, "client udp port %d\n", ntohs(servAddr.sin_port)); 
    }

    set_listen_port(ntohs(servAddr.sin_port));
    
    return 0; 
}

void client::populate_self(const char *input)
{
    char buffer[1024];
    strcpy(buffer, input);

    pid = getpid();

    (uint32_t)strtoul(buffer, NULL, 0);
    char *pch = NULL;

    pch = strtok(buffer, "\n");
    nonce = (uint32_t) strtoul(pch, NULL, 0);
 
    pch = strtok(NULL, "\n");
    strcpy(name, pch);

    if (strncmp(name, "logmsg", strlen("logmsg")) == 0) // special node which needs to log
        log_all = true;

    pch = strtok(NULL, "\n");
    fc_port = (uint32_t) strtoul(pch, NULL, 0);


    pch = strtok(NULL, "\n");
    strcpy(fc_name, pch);

    hashval = nonce_name_hash(nonce, name);

    fprintf(stdout, "Client: %s hash: 0x%X\n", name, hashval);
    // fprintf(stdout, "       Process: %d Nonce: %u fs: %s fp: %u\n", pid, nonce, fc_name, fc_port);
    if (fc_port == 0)
	{
		first_client = true;
	}

    generate_file_name_and_open();
}

int client::select_loop()
{
    struct timeval tv;
    fd_set readfds;
    int err = 0;

    int mgr_sock = get_mgr_sock();
    int udp_sock = get_listen_sock();
    int max_val = ((mgr_sock > udp_sock)? mgr_sock+1:udp_sock+1);

    tv.tv_sec  = 1500; // how often should I timeout?
    tv.tv_usec = 0;

    int count = 0;

    //if (first_client)
        fprintf(stdout, "%s: Manager Socket: %d\n", name, mgr_sock);
        fprintf(stdout, "%s: SelfUDP Socket: %d\n", name, udp_sock);

    //FD_ZERO(&readfds);
    //FD_SET(udp_sock, &readfds);
    //FD_SET(mgr_sock, &readfds);
    

    while (1) {

        FD_ZERO(&readfds);
        FD_SET(udp_sock, &readfds);
        FD_SET(mgr_sock, &readfds);

    	err = select( max_val, //mgr_sock+ 1+udp_sock,
    			&readfds,
    			NULL,
    			NULL,
    			&tv	);
    	if (err == -1) {
    		perror("select failed!!\n");
    	}  	
        else if (err) {
    		// fprintf(stdout, "data available!\n");

    		if (FD_ISSET(mgr_sock, &readfds)) {
    			read_from_control();// read
    		}

    		if (FD_ISSET(udp_sock, &readfds)) {
    			read_from_ring();
    		}

    	} 	else  {
    		fprintf(stdout, "timed out data not available within 2 secs!\n");
    	}
    	count += 1;
    	if (count == 1000)
    		break;
    }

    //print_finger_table();
    return 0;
}

int client::read_from_control()
{
    char buffer[1024];

    //struct sockaddr_in servaddr,cliaddr;
    //socklen_t len = sizeof(cliaddr);

    memset(buffer, 0, sizeof(buffer));

    fprintf(stdout, "%s: Select detected activity on Control port\n", name);

    int n = recv(mgr_sock, buffer, sizeof(buffer)-1, 0);
    if (n < 0) {
        fprintf(stderr, "spurious wakeup: nothing to read from manager\n");
    } else {
        char *pch = strtok(buffer, " ");

        if ( strncmp (pch, "store", strlen("store")) == 0)  {

            pch = strtok(NULL, "\n");
            fprintf(stdout, "received STORE %s message from Manager\n", pch);
            process_tcp_store_cmd(pch);

        } else if (strncmp (pch, "search", strlen("search")) == 0) {

            pch = strtok(NULL, "\n");
            fprintf(stdout, "received SEARCH %s message from Manager\n", pch);
            process_tcp_search_cmd(pch);
        
        } else if (strncmp (pch, "dump", strlen("dump")) == 0) {

            //pch = strtok(NULL, "\n");
            fprintf(stdout, "received DUMP %s message from Manager\n", pch);
            //process_tcp_dump_cmd(pch);

            FILE *fp = fopen("dump.txt", "a");
            print_finger_table(fp);
            fclose(fp);

            send_ok();

        } else if (strncmp (pch, "exit", strlen("exit")) == 0) {

            //pch = strtok(NULL, "\n");
            fprintf(stdout, "received %s message from Manager\n", pch);
            //process_tcp_dump_cmd(pch);
            send_ok();

            cleanup_and_exit();
          
            
        } else if (strncmp (pch, "end_client", strlen("end_client")) == 0) {

            pch = strtok(NULL, "\n");
            fprintf(stdout, "received end_client %s message from Manager\n", pch);
            process_tcp_leaving_cmd(pch);
        }
    }
    return 0;
}


int client::read_from_ring()
{
	uint8_t buffer[1024];

	struct sockaddr_in /*servaddr,*/ cliaddr;

	recv_udp_data(buffer, sizeof(buffer), &cliaddr); // does memset as reqd

	mtype_t *t = (mtype_t *)buffer;

	switch(t->type)
	{
		case SUC_Q:
			//fprintf(stdout, "OHOHOHOHOHOHOHOHOHOHOHO coooooooooooooool\n");
			rcv_sucpred_q(buffer);
			send_sucpred_r(SUC_Q, ntohs(cliaddr.sin_port));
			break;
		case PRE_Q:
			//fprintf(stdout, "OHOHOHOHOHOHOHOHOHOHOHO coooooooooooooool\n");
			rcv_sucpred_q(buffer);
			send_sucpred_r(PRE_Q, ntohs(cliaddr.sin_port));
			break;
		case UPD_Q:
			//fprintf(stdout, "OHOHOHOHOHOHOHOHOHOHOHO coooooooooooooool\n");
			updr_t reply;
			rcv_upd_q(buffer, &reply); // populate fields of reply
			send_upd_r(ntohs(cliaddr.sin_port), &reply);
			break;
		case STS_Q:
			//fprintf(stdout, "OHOHOHOHOHOHOHOHOHOHOHO coooooooooooooool\n");
			stores_q_t storesq;
			stores_r_t storesr;

			rcv_stores_q(buffer, &storesq); // populate fields of reply
			
            if (get_stage() < 4)
                populate_stores_reply(&storesq, &storesr);
            else
                populate_stores_reply_stage4(&storesq, &storesr);

			send_stores_r(ntohs(cliaddr.sin_port), &storesr);
			break;
		case STO_Q:
			store_q_t storeq; memset(&storeq, 0, sizeof(store_q_t));
			store_r_t storer; memset(&storer, 0, sizeof(store_r_t));

			rcv_store_q(buffer, &storeq); // populate fields of reply
			populate_store_reply(&storeq, &storer);
			send_store_r(ntohs(cliaddr.sin_port), &storer);
			break;
        case LEV_Q:
            lv_q leave_q;
            uint32_t his_port;
            
            his_port = ntohs(cliaddr.sin_port);
            rcv_leaving_q(buffer, &leave_q);
            fprintf(stdout, "_____________________________________________\n");

            if (leave_q.di == pre_id)
                send_next_data_q();
            else
                process_random_leaving(leave_q.di , his_port);
            //process_leaving_q(&leave_q);
            break;

        case LEV_R:
            /*lv_r leave_r;
            rcv_leaving_r(buffer, &leave_r);

            uint32_t responder_port;
            responder_port = ntohs(cliaddr.sin_port);
            // is this from my successor or other clients?
            if (responder_port == suc_port) {
                READY_TO_EXIT1 = 1;
            } else {
                find_and_erase_port(responder_port);
                if (vec_ports.size() == 0)
                    READY_TO_EXIT2 = 1;
            }

            if (READY_TO_EXIT1 && READY_TO_EXIT2) {
                
            }*/

            break;
        case NDT_Q:
            nd_q dtq; memset(&dtq, 0, sizeof(nd_q));
            rcv_next_data_q(buffer, &dtq);
            send_next_data_r(&dtq);
            break;
        case NDT_R:
            nd_r dtr;
            RECEIVED_ALL_DATA = rcv_next_data_r(buffer, &dtr);
            if (RECEIVED_ALL_DATA)
                process_predecessor_leaving();  // also sends last reply
            else
                send_next_data_q();
            break;

		default:

			fprintf(stdout, "Unknown TRIAD message received\n");
			break;
	}
	return 0;
}

void client::populate_stores_reply(stores_q_t *pq, stores_r_t *pr)
{
    if (pq->ni == hashval) // are you asking me?
    {
        /*(hashval < suc_id && hashval < pre_id && 
                       ( datahash > pre_id || datahash <= hashval ) );*/

        int that_condition = (hashval < suc_id && hashval < pre_id && 
                    ( pq->di > pre_id || pq->di <= hashval ) );
        pr->ni = pq->ni;
        pr->di = pq->di;
        //if ( (pq->di > pre_id && pq->di <= hashval ) || that_condition )
        if ( (pq->di > pre_id && pq->di <= hashval ) || that_condition )     
        {
            pr->ri = hashval;
            pr->rp = my_udp_port;

            if(is_hash_present(pq->di))
                pr->has = 1;
            else
                pr->has = 0;
        }
        else
        {
            pr->ri = suc_id;
            pr->rp = suc_port;
            pr->has = 0;
        }

    }
    else
    {
        fprintf(stderr, "%s: my hashval received:0x%x", name, pq->ni);
        die ("stores_q was not meant for me!!\n");
    }
}

void client::populate_store_reply(store_q_t *pq, store_r_t *pr)
{
    if (pq->ni == hashval) // are you asking me?
    {
        // Am I leaving? stage 5 pr->R = 0; maybe

        uint32_t datahash = nonce_name_hash(nonce, pq->S);
        int that_condition = ( hashval < suc_id && hashval < pre_id && 
                       ( (datahash > pre_id) || datahash <= hashval ) );
        
        pr->ni = pq->ni;
        pr->SL = pq->SL;
        strcpy(pr->S, pq->S);

        if ( (datahash > pre_id && datahash <= hashval ) || that_condition )
             
        {
            if(is_hash_present(datahash))
                pr->R = 2;
            else {
                pr->R = 1;
                data_vec.push_back(data(pq->S, datahash));
            }
        }
    }
    else
    {
        fprintf(stderr, "%s: ", name);
        die ("store_q was not meant for me!!\n");
    }
}

int client::send_ok()
{
    char okmsg[20];
    int sockfd = get_mgr_sock();
    strcpy(okmsg, "OK\n");
    int n = write(sockfd, okmsg, strlen(okmsg));
    if (n < 0) {
        fprintf(stderr, "Client: %s", name);
        die("could not send OK!!\n");
    }

    return 0;
}

int client::establish_ring_with_pred_succ()
{
    uint8_t buffer[1024];
    uint32_t next_id, next_port;
    //uint32_t prev_id, prev_port;
    uint32_t cur, cur_port;
    struct sockaddr_in cliaddr;
    int status = 0;

    if (fc_id == hashval)
        die("First client with the same name exists\n");

    send_sucpred_q(SUC_Q, fc_id, fc_port); // query the first client for its successor
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &next_id, &next_port);

    if (fc_id == next_id)
    {
        // Only one client
        printf("Entered Single Node case: %s 0x%x\n", name, hashval);
        pre_id = suc_id = fc_id;
        pre_port = suc_port = fc_port;

        updq_t pkt(UPD_Q, fc_id, hashval, my_udp_port, 0);
        send_upd_q(fc_port, &pkt); // make hashval predecessor of first
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_upd_r(buffer, &status);

        pkt.i = 1;
        send_upd_q(fc_port, &pkt); // make hashval successor of first
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_upd_r(buffer, &status);

        fprintf(stdout, "Single node case Updates Completed!\n");

        return 0;
    }

    printf("Entered Multi Node case: %s 0x%x\n", name, hashval);

    /*send_sucpred_q(PRE_Q, fc_id, fc_port); // query the first client for its predecessor
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &prev_id, &prev_port);*/

    cur = fc_id; cur_port = fc_port;

    //algorithm
    
    while (!inOpenRange(hashval, cur, next_id))
    {
        cur = next_id; cur_port = next_port;

        send_sucpred_q(SUC_Q, cur, cur_port); // query the first client
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_sucpred_r(buffer, &next_id, &next_port);
    }
        
    pre_id = cur;
    pre_port = cur_port;
    suc_id = next_id;
    suc_port = next_port;


    updq_t pkt(UPD_Q, suc_id, hashval, my_udp_port, 0);
    send_upd_q(suc_port, &pkt); // make hashval predecessor of first
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_upd_r(buffer, &status);

    pkt.i = 1; pkt.ni = pre_id;
    send_upd_q(pre_port, &pkt); // make hashval successor of first
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_upd_r(buffer, &status);


    fprintf(stdout, "Multi node case Updates Completed!\n");

    return 0;
}

int client::establish_ring_with_pred_succ_stage4()
{
    // stage 4
    // Here we should not inform our predecessor to update his successor pointer to me.

    uint8_t buffer[1024];
    uint32_t next_id, next_port;
    uint32_t prev_id, prev_port, cur, cur_port;
    struct sockaddr_in cliaddr;

    if (fc_id == hashval)
        die("First client with the same name exists\n");

    send_sucpred_q(SUC_Q, fc_id, fc_port); // query the first client for its successor
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &next_id, &next_port);

    if (fc_id == next_id)
    {
        // Only one client
        printf("Entered Single Node case: %s 0x%x\n", name, hashval);
        pre_id = suc_id = fc_id;
        pre_port = suc_port = fc_port;

        fprintf(stdout, "Single node case Updates Completed!\n");

        fprintf(stdout, "%s: pre_id: 0x%x  suc_id: 0x%x\n", name, pre_id, suc_id);
//        sleep(10);
        return 0;
    }

    printf("Entered Multi Node case: %s 0x%x\n", name, hashval);

    send_sucpred_q(PRE_Q, fc_id, fc_port); // query the first client for its predecessor
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &prev_id, &prev_port);

    cur = fc_id; cur_port = fc_port;

    //algorithm
    
    while (!inOpenRange(hashval, cur, next_id))
    {
        cur = next_id; cur_port = next_port;

        send_sucpred_q(SUC_Q, cur, cur_port); // query the first client
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_sucpred_r(buffer, &next_id, &next_port);
    }

        
    pre_id = cur;
    pre_port = cur_port;
    suc_id = next_id;
    suc_port = next_port;
    
    fprintf(stdout, "Multi node case Updates Completed!\n");

    // predecessor's tables are updated using update_finger_table()
    fprintf(stdout, "%s: pre_id: 0x%x  suc_id: 0x%x\n", name, pre_id, suc_id);

    return 0;
}

void client::generate_file_name_and_open()   // for logging
{
    sprintf(logfile, "stage%d.%s.out", get_stage(), name);
    fptr = fopen(logfile, "w");
    if (NULL == fptr) {
        fprintf(stderr, "Unable to create/open the logfile: %s\n", logfile);

    }
}

void client::cleanup_and_exit()
{
    //fprintf(stdout, "!!!!!!!Client %s- Exiting!!!!!!\n", name);    
    if (fptr)
        fclose(fptr);

    close(my_listen_sock);
    close(mgr_sock);

    fprintf(stdout, "!!!!!!!Client %s- Exiting!!!!!!\n", name);
    exit(0);
}

void client::die(const char *s)
{
    fprintf(stderr, "%s\n", s);
    cleanup_and_exit();
}
