#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>
#include <cmath>

#include "client.h"

using namespace std;

void client::setup_finger_tables()
{
    uint8_t buffer[1024];
    struct sockaddr_in cliaddr;

    fprintf(stdout, "%s: setup_finger_tables\n", name);
    if (!first_client)
    {
        init_finger_table();

        update_others(0);   // 0 when node joining, 1 when I need to take care of 
                            // predecessor leaving the ring

        updq_t pkt(UPD_Q, pre_id, hashval, my_udp_port, 1);
        int status = 0;
    //>>>>>>>>>>
        /*send_upd_q(pre_port, &pkt); // make current client the successor of predecessor
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_upd_r(buffer, &status);*/
    //>>>>>>>>>>
        pkt.i = 0; pkt.ni = suc_id;
        send_upd_q(suc_port, &pkt); // make current client the predecessor of successor
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_upd_r(buffer, &status);
    }
    else
    {
        for (int i=1; i <= MAX_FT_ENTRIES; ++i)
        {
            ft.at(i).start = ft.at(i).begin = hashval + pow(2, i-1);
            ft.at(i).end = hashval + pow(2, i);
            ft.at(i).id = hashval;  // x.node = n
            ft.at(i).id_port = my_udp_port;
        }

        //ft.at(MAX_FT_ENTRIES).end -= 1;
    }
    
    ft.at(0).id = pre_id; ft.at(0).id_port = pre_port; // predecessor

    //print_finger_table();
    
}

void client::init_finger_table()
{
    // you already know some ndash -- fc_id and fc_port
    // you have found suc_id, pre_id and updated them
    fprintf(stdout, "%s: init_finger_table\n", name);
    for (int i = 1; i <= MAX_FT_ENTRIES; ++i)
    {
        ft.at(i).start = ft.at(i).begin = hashval + (1 << (i-1) );
        ft.at(i).end = hashval + (1 << i);
    }
    ft.at(1).id = suc_id; ft.at(1).id_port = suc_port;

    
    int m = MAX_FT_ENTRIES;

    for (int i = 1; i < m; ++i)
    {
        if (inLeftClosedRange (ft.at(i + 1).start, hashval, ft.at(i).id) )
        {
            ft.at(i + 1).id = ft.at(i).id;
            ft.at(i + 1).id_port = ft.at(i).id_port;
        }   
        else
        {
            uint32_t fs_id = 0, fs_port = 0;

            find_successor_from_hint(ft.at(i + 1).start, &fs_id, &fs_port,
                ft.at(i).id, ft.at(i).id_port);

            ft.at(i + 1).id = fs_id;
            ft.at(i + 1).id_port = fs_port;
        } 
    }
}

void client::print_finger_table(FILE *fp)
{
    fprintf(fp, "Finger Table of %s ID:0x%x\n", name, hashval);
    fprintf(fp, "start \t begin \t end  \t id \t id_port \n");
    for (int i = 0; i <= MAX_FT_ENTRIES; ++i)
    {
        fprintf(fp, "0x%x \t 0x%x \t 0x%x  \t 0x%x \t %d \n", 
                ft.at(i).start, ft.at(i).begin, ft.at(i).end, ft.at(i).id, ft.at(i).id_port);
    }
}

void client::update_others(int mode)
{
    fprintf(stdout, "%s: update_others\n", name);
    uint32_t param = hashval;
    
    if (mode)   //  node_leaving scenario
        param = pre_id;

    fprintf(stdout, "%s: update_others\n", name);
    for (int i = 1; i <= MAX_FT_ENTRIES; ++i)
    {
        uint32_t p = 0, p_port = 0;
        uint32_t temp = param - (uint32_t) pow(2, i-1);

        find_predecessor(temp, &p, &p_port);
        update_finger_tables(p, p_port, i);
    }
}

void client::update_finger_tables(uint32_t p_id, uint32_t p_port, int i)
{
    fprintf(stdout, "%s: update_finger_tables\n", name);
    uint8_t buffer[1024];
    struct sockaddr_in cliaddr;
    uint32_t temp_id = p_id;
    uint32_t temp_port = p_port;
    updq_t pkt(UPD_Q, p_id, hashval, my_udp_port, i);
    int status = 0;

    while (1) {
        // send update-q (pre_id, s, s_port, i)
    
        send_upd_q(temp_port, &pkt);
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_upd_r(buffer, &status);

        if (status == 0)
            break;

        send_sucpred_q(PRE_Q, temp_id, temp_port);
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_sucpred_r(buffer, &temp_id, &temp_port);
        pkt.ni = temp_id;
    }
}

void client::find_successor(uint32_t id, uint32_t *res_id, uint32_t *res_port)
{
    // makes use of finger table
    fprintf(stdout, "%s: find_successor\n", name);
    
    /*int that_condition = (hashval < suc_id && hashval < pre_id && 
                       ( (id > pre_id) || id <= hashval ) );*/

    // first client stores data; or if it's the only client in ring
    // if (suc_id == hashval || (id > pre_id && id <= hashval) || that_condition) {
    if (suc_id == hashval || inRightClosedRange(id, pre_id, hashval) ) {
        *res_id = hashval;
        *res_port = my_udp_port;
    } else {

        uint32_t temp_id = 0, temp_port = 0;
        
        lookup_finger_table(id, &temp_id, &temp_port);
        fprintf(stdout, "Client %s: Look up finger table id:0x%x res: 0x%x %d\n", 
                name, id, temp_id, temp_port);

        // same algo as below
        find_successor_from_hint(id, res_id, res_port, temp_id, temp_port);
    }
}

void client::find_successor_from_hint(uint32_t id, uint32_t *res_id, uint32_t *res_port, 
    uint32_t hint, uint32_t hint_port)
{
    fprintf(stdout, "%s: find_successor_from_hint\n", name);
    uint8_t buffer[1024];
    struct sockaddr_in cliaddr;

/*int that_condition = (hashval < suc_id && hashval < pre_id && 
                       ( (id > pre_id) || id <= hashval ) );
*/
    
    fprintf(stdout, "%s: hashval - 0x%x pre_id - 0x%x suc_id - 0x%x id - 0x%x hint-0x%x\n", 
            name, hashval, pre_id, suc_id, id, hint);
//    sleep(20);

    // first client stores data; or if it's the only client in ring
    //if (suc_id == hashval || (id > pre_id && id <= hashval) || that_condition)
    if (suc_id == hashval || inRightClosedRange(id, pre_id, hashval) )
    {
        *res_id = hashval;
        *res_port = my_udp_port;
    }
    else
    {
        // if prev check failed and hint was myself, then lets roll with successor
        if (hint == hashval)
        {
            hint = suc_id;
            hint_port = suc_port;
            // trick is to never query yourself
        }

        int looped = 0;
        stores_r_t sr;
        uint32_t tmp_port = hint_port;
        stores_q_t sq; sq.di = id; sq.ni = hint;
        while (1) { 

            send_stores_q(tmp_port, &sq);
            recv_udp_data(buffer, sizeof(buffer), &cliaddr);
            rcv_stores_r(buffer, &sr);
            if (sr.ni == sr.ri) {
                
                *res_id = sr.ri;
                *res_port = sr.rp;
                break;
            }

            // prepare next stores-q
            sq.ni = sr.ri; tmp_port = sr.rp;
            if (sq.ni == hashval)
                looped++;

            if (looped > 1)
            {
                /*if (is_hash_present(datahash)) {
                    fprintf(stdout, "Search Data %s with hash 0x%x, FOUND in 0x%x\n",
                        S, datahash, hashval);
                }*/
                *res_id = hashval;
                *res_port = my_udp_port;
            }
        }
    }
}

void client::find_predecessor(uint32_t id, uint32_t *res, uint32_t *res_port)
{
    fprintf(stdout, "%s: find_predecessor - id 0x%x\n", name, id);
    uint32_t temp_id = 0, temp_port = 0;
    struct sockaddr_in cliaddr;
    uint8_t buffer[1024];

    find_successor(id, &temp_id, &temp_port);
    if (temp_id && temp_port) {
        
        if (temp_id == hashval) {
            *res = pre_id;
            *res_port = pre_port;
        } else {
            // send predecessor

            send_sucpred_q(PRE_Q, temp_id, temp_port);
            recv_udp_data(buffer, 1024, &cliaddr);
            rcv_sucpred_r(buffer, res, res_port);
        }
    }
    else
    {
        fprintf(stderr, "Client: %s - find_predecessor went wrong\n", name);
        die ("Exiting\n");
    }
    fprintf(stdout, "Client: %s - find_predecessor returns 0x%x\n", name, *res); 
}

void client::populate_stores_reply_stage4(stores_q_t *pq, stores_r_t *pr)
{
    fprintf(stdout, "%s: populate_stores_reply_stage4\n", name);
    if (pq->ni == hashval) // are you asking me?
    {
        /*int that_condition = (hashval < suc_id && hashval < pre_id && 
                    ( pq->di > pre_id || pq->di <= hashval ) );*/
        pr->ni = pq->ni;
        pr->di = pq->di;

        fprintf(stdout, "%s: pre_id: 0x%x  suc_id: 0x%x\n", name, pre_id, suc_id);

        //if ( (pq->di > pre_id && pq->di <= hashval ) || that_condition )
        if ( (inRightClosedRange(pq->di, pre_id, hashval) ) || ( pre_id == hashval) )     
        {
            pr->ri = hashval;
            pr->rp = my_udp_port;

            if(is_hash_present(pq->di))
                pr->has = 1;
            else
                pr->has = 0;
        }
        else
        {
            lookup_finger_table(pq->di, &pr->ri, &pr->rp);
            
            pr->has = 0;
        }
    }
    else
    {
        fprintf(stderr, "%s: my hashval received:0x%x", name, pq->ni);
        die ("stores_q was not meant for me!!\n");
    }
}

void client::lookup_finger_table(uint32_t id, uint32_t *res, uint32_t *res_port)
{
    fprintf(stdout, "%s: lookup_finger_table\n", name);
    int flag = 0;
    for (int i = 1; i <= MAX_FT_ENTRIES; ++i)
    {
        if (inLeftClosedRange(id, ft.at(i).begin, ft.at(i).end))
        {
            *res = ft.at(i).id;
            *res_port = ft.at(i).id_port;
            flag = 1;
            break;
        }
    }
    if (!flag)
    {
        *res = suc_id;
        *res_port = suc_port;
    }
}
