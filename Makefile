CC=g++
CFLAGS=-c -Wall -g

all:projb clogs

projb:server.o handlers.o more.o client.o common.o sha1.o 
	$(CC) -o projb -g server.o client.o handlers.o more.o common.o sha1.o 

server.o:server.cpp server.h
	$(CC) $(CFLAGS) server.cpp

client.o:client.cpp client.h
	$(CC) $(CFLAGS) client.cpp 

handlers.o:handlers.cpp client.h
	$(CC) $(CFLAGS) handlers.cpp

more.o:more.cpp client.h
	$(CC) $(CFLAGS) more.cpp

sha1.o: sha1.c sha1.h
	$(CC) $(CFLAGS) sha1.c

common.o:common.cpp common.h sha1.h
	$(CC) $(CFLAGS) common.cpp

clean:
	rm -f projb *.o

clogs:
	rm -f stage*.*.out
