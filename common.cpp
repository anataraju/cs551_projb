/*
 * common.cpp
 *
 *  Created on: Oct 25, 2013
 *      Author: amogh
 */
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string.h>

#include "sha1.h"
#include "common.h"

using namespace std;

void die(const char *s)
{
	fprintf(stderr, "%s\n", s);
	exit(1);
};

int stripLastNewline(char *buffer) {
    //fprintf(stdout, "Before %s\n", buffer);
    char *pch = buffer;
    if (pch == NULL) {
        fprintf(stderr, "Empty string passed to stripLastNewline\n");
        return -1;
    }

    while (*pch != '\n') {
        pch++;
    }
    if (*pch == '\n')
        *pch = '\0';

    //fprintf(stdout, "After %s", buffer);
    return 0;
}

unsigned int nonce_name_hash(int nonce, char *name)
{
    int name_length = strlen(name);
    int buffer_length = sizeof(int) + name_length;
    unsigned char *buffer = (unsigned char *)malloc(buffer_length);
    int int_size = (int)sizeof(int);
    int i = 0;
    for (; i < int_size; ++i)
        buffer[i] = nonce >> (8 * (int_size - i - 1));

    memcpy(buffer + i, (unsigned char *)name, strlen(name));

    /*printf("Concatenated input:");
    for (i = 0; i < buffer_length; ++i)
        printf("%02x ", buffer[i]);
    printf("\n");*/

    //die("you need to fill in this part of the function to build\n the buffer as per the project specification.\n");

    unsigned int result = projb_hash(buffer, buffer_length);
    free(buffer);

    // printf("Result: 0x%x\n", result);
    return result;
}


// these function take take of wrapping around after uint32_t limit

int inOpenRange(uint32_t val, uint32_t lb, uint32_t ub)
{
    if (lb < ub) return (lb < val && val < ub);
    else if (lb > ub) return (lb < val || val < ub);
    else return 0;  // lb == ub
}

int inRightClosedRange(uint32_t val, uint32_t lb, uint32_t ub)
{
    if (lb < ub) return (lb < val && val <= ub);
    else if (lb > ub) return (lb < val || val <= ub);
    else return 0;  // lb == ub
}

int inLeftClosedRange(uint32_t val, uint32_t lb, uint32_t ub)
{
    if (lb < ub) return (lb <= val && val < ub);
    else if (lb > ub) return (lb <= val || val < ub);
    else return 0;  // lb == ub
}

void printRaw(FILE *fp, uint8_t *buf, size_t size, int chars)
{
    fprintf(fp, "raw:  ");
    size_t end = ((chars > 0)? (size-80):size);
    size_t i = 0;

    for (i = 0; i < end; i+=4)
        fprintf(fp, "%02x%02x%02x%02x", buf[i+3], buf[i+2], buf[i+1], buf[i]);

    i = end;
    while (chars) {
        fprintf(fp, "%02x", buf[i++]); --chars;
    }
    fprintf(fp, "\n");
}