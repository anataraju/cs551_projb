#include <iostream>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <vector>
#include <set>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <algorithm>
#include <netdb.h>

#include "common.h"

using namespace std;

#define MAX_FT_ENTRIES 32

enum TRIAD_MSG_TYPES {
	SUC_Q = 1,
	SUC_R,
	PRE_Q,
	PRE_R,
	STS_Q,
	STS_R,
	UPD_Q,
	UPD_R,
	STO_Q,
	STO_R,
    LEV_Q,
    LEV_R,
    NDT_Q,
    NDT_R
};

typedef struct sucq{
	uint32_t type;
	uint32_t ni;
} sucq_t;

typedef struct succr{
	uint32_t type;
	uint32_t ni;
	uint32_t si;
	uint32_t sp;
} sucr_t;

typedef struct updq{
	uint32_t type;
	uint32_t ni;
	uint32_t si;
	uint32_t sp;
	uint32_t i;

    updq(uint32_t t, uint32_t n,uint32_t sri,uint32_t srp,uint32_t ii){
        type = t; ni = n; si = sri; sp = srp;  i = ii;
    }
} updq_t;

typedef struct updr {
	uint32_t type;
	uint32_t ni;
	uint32_t R;
	uint32_t si;
	uint32_t sp;
	uint32_t i;
} updr_t;

struct lv_q {
    uint32_t type, ni, di;
};

struct lv_r {
    uint32_t type, ni;
};

struct nd_q {
    uint32_t type, di, id;
};

struct nd_r {
    uint32_t type, di, qid, rid, SL;
    char S[80];
    //nd_r() { memset (&S, 0, 80); }
};

typedef struct mtype {
	uint32_t type;
} mtype_t;


typedef struct stores_q {
    uint32_t type, ni, di;
} stores_q_t;

typedef struct stores_r {
    uint32_t type, ni, di, ri, rp, has;
} stores_r_t;

typedef struct store_q {
    uint32_t type, ni, SL;
    char S[80];
    //store_q() { memset (&S, 0, 80); }
} store_q_t;

typedef struct store_r {
    uint32_t type, ni, R, SL;
    char S[80];
    //store_r() { memset (&S, 0, 80); }
} store_r_t;

class data {
public:
	char ddata[80];
	uint32_t dhash;

	data(char *s, uint32_t dh){ strcpy (ddata, s); dhash = dh; }
	data & operator=(const data &d) { strcpy (ddata, d.ddata); dhash = d.dhash; return *this; }
	data(const data &d) { *this = d; }
};

class entry {
public:
    uint32_t start, begin, end, id, id_port;
    entry() { start = begin = end = id = id_port = 0; }
    entry(uint32_t s, uint32_t b, uint32_t e, uint32_t i, uint32_t ip) {
        start = s; begin = b; end = e; id = i; id_port = ip;
    }
    entry & operator=(const entry &k) { start = k.start; begin = k.begin; end = k.end; id = k.id; id_port = k.id_port; return *this;}
    entry (const entry & b) { *this = b; }

};

class client {

protected:
    int mgr_port;   // tcp - with manager the connection 
                    // is transient
    int stage;

    int mgr_sock;   // tcp socket for control data
    uint32_t suc_port; // successor's port
    uint32_t suc_id;
    uint32_t pre_port; // predeccessor's port
    uint32_t pre_id;
    uint32_t my_udp_port;
    int my_listen_sock;

    // for logging
    FILE *fptr;
    char logfile[MAX_FILE_NAME_LENGTH];

    uint32_t nonce; 
    char name[MAX_CLIENT_NAME_LENGTH];
    uint32_t fc_port;
    char fc_name[MAX_CLIENT_NAME_LENGTH];
    pid_t pid;
    uint32_t hashval, fc_id;

    vector<data> data_vec;
    vector<entry> ft;
    vector<uint32_t> vec_ports;
    vector<uint32_t> vec_ids;

    bool first_client;
    bool log_all;
    int  RECEIVED_ALL_DATA;
    int READY_TO_EXIT1;
    int READY_TO_EXIT2;

public:
    client() {}

    client( int port, int stg) { 
        mgr_port = port;
        stage = stg;
        first_client = false;

        suc_port = pre_port = mgr_sock = fc_port = 0;
        suc_id = pre_id = 0;
        strcpy(name, ""); strcpy(fc_name, "");
        log_all = false;
        hashval = 0;

        data_vec.clear(); ft.clear(); vec_ports.clear(); vec_ids.clear();

        for (int i = 0; i <= MAX_FT_ENTRIES; ++i)
            ft.push_back(entry()); 

        RECEIVED_ALL_DATA = 0;

        READY_TO_EXIT1 = 0;
        READY_TO_EXIT2 = 0;

        fptr = NULL;


    }

    ~client() { }

    // Ring maintenance
    int establish_ring_with_pred_succ();

    int select_loop();  // select
    int read_from_ring();   // handle activities on triad UDP sock
    // handle activities on manager TCP sock
    int read_from_control();
    void process_tcp_store_cmd(char *S);
    void process_tcp_search_cmd(char *S);
    void process_tcp_leaving_cmd(char *S);

    // getters/setters
    void set_listen_sock(int ls) {my_listen_sock = ls;}
    void set_listen_port(uint32_t p) {my_udp_port = p;}
    void set_mgr_sock (int x) { mgr_sock = x; }
    int get_mgr_sock () { return mgr_sock; }
    int get_listen_sock() { return my_listen_sock; }
    int get_stage() { return stage; }

    void start();

    int child_proc();
    void populate_self(const char *buf); // read control info from manager and populate initial data
    int open_udp_server_sock();
    
    //void (uint32_t ni);	// where will I get the port of that node? - update
    // message_handlers
    void send_sucpred_q(int type/* succ/pred*/, uint32_t dest_id, uint32_t dest_port);
    void rcv_sucpred_q(uint8_t * rcvbuf /*input*/);
    void send_sucpred_r(int type, uint32_t dest_port);	// get port from decoded client addr
    void rcv_sucpred_r(uint8_t *rcvbuf /*input*/, uint32_t *ri, uint32_t *rp);
    void recv_udp_data(uint8_t *buffer, size_t bufflen, struct sockaddr_in *cliaddr);
    void recv_tcp_data(uint8_t *buffer, size_t bufflen);
    int  send_ok();
    
    void send_upd_q(uint32_t dest_port, updq_t *query);
    void rcv_upd_r(uint8_t *rcvbuf, int *status);
    int  rcv_upd_q(uint8_t *buffer, updr_t *reply);
    void send_upd_r(uint32_t dest_port, updr_t *reply);


    void send_stores_q(uint32_t dst_port, stores_q_t * pq);
    void rcv_stores_q(uint8_t *buf, stores_q_t * pq);
    void send_stores_r(uint32_t dst_port, stores_r_t * pr);
    void rcv_stores_r(uint8_t *buf, stores_r_t * pr);
    void send_store_q(uint32_t dst_port, store_q_t * pq);
    void rcv_store_q(uint8_t *buf, store_q_t * pq);
    void send_store_r(uint32_t dst_port, store_r_t * pr);
    void rcv_store_r(uint8_t *buf, store_r_t * pr);
    void populate_stores_reply(stores_q_t *pq, stores_r_t *pr);
    void populate_store_reply(store_q_t *pq, store_r_t *pr);
    int is_hash_present(uint32_t hash);

    // stage 4 specific
    int establish_ring_with_pred_succ_stage4(); 
    // it won't update
    // predecessor.successor to myself
    void setup_finger_tables();
    void init_finger_table();
    void print_finger_table(FILE *fp);
    void update_others(int mode);
    void update_finger_tables(uint32_t p_id, uint32_t p_port, int i);
    void find_successor(uint32_t id, uint32_t *res_id, uint32_t *res_port);    
    void find_successor_from_hint(uint32_t id, uint32_t *res_id, uint32_t *res_port, uint32_t hint, uint32_t hint_port);
    void find_predecessor(uint32_t id, uint32_t *res, uint32_t *res_port);
    void populate_stores_reply_stage4(stores_q_t *pq, stores_r_t *pr);
    void lookup_finger_table(uint32_t id, uint32_t *res, uint32_t *res_port);


    // stage 5
    void send_leaving_q(uint32_t to_id, uint32_t to_port);
    void rcv_leaving_q(uint8_t *buf, lv_q *pq);
    void process_leaving_q(lv_q *pq);
    void send_leaving_r(uint32_t dest_id, int dest_port);
    void rcv_leaving_r(uint8_t *buf, lv_r *pq);
    void send_next_data_q();
    void rcv_next_data_q(uint8_t *buf, nd_q *pq);
    int rcv_next_data_r(uint8_t *buf, nd_r *pdr);
    int send_next_data_r(nd_q * q); 

    void process_predecessor_leaving();
    void process_random_leaving(uint32_t, uint32_t);
    void find_and_erase_port(uint32_t);

    void generate_file_name_and_open();
    void cleanup_and_exit();
    void die(const char *s);

};

/*typedef struct anydata {
	uint32_t ni, R, si, sp, i;

	anydata() {
		ni = R = si = sp = i = 0;
	}
} anydata_t;*/

