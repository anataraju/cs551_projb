/*
 * common.h
 *
 *  Created on: Oct 25, 2013
 *      Author: amogh
 */

#ifndef COMMON_H_
#define COMMON_H_

using namespace std;

#define MAX_CLIENT_NAME_LENGTH 81
#define MAX_FILE_NAME_LENGTH 512

typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;


unsigned int nonce_name_hash(int nonce, char *name);
void die(const char *s);

int inOpenRange(uint32_t val, uint32_t lb, uint32_t ub);    // (a,b)
int inRightClosedRange(uint32_t val, uint32_t lb, uint32_t ub);  // (a,b]
int inLeftClosedRange(uint32_t val, uint32_t lb, uint32_t ub);   // [a,b)

void printRaw(FILE *fp, uint8_t *buf, size_t size, int chars = 0);

#endif /* COMMON_H_ */
