#include <iostream>
#include <cstdlib>

using namespace std;

typedef unsigned long uint32_t;

void print_sizes() {
    cout << "ulong:" << sizeof(unsigned long) << "\tuint:" << sizeof(unsigned int) << endl;
    cout << "long: " << sizeof(long) << "\tlong long: " << sizeof(long long) << endl;

    cout << strtoul("123456789", NULL, 0) << endl;
    
    uint32_t a = 0x3, b = 0xfffcd, c = 0xabcdef;
    cout << "a:" << a << " > b:" << b << "    ans:" <<((a > b)?1:0) << endl;
    cout << "b:" << b << " > c:" << c << "    ans:" <<((b > c)?1:0) << endl;
    cout << "c:" << c << " > a:" << a << "    ans:" <<((c > a)?1:0) << endl;
}

int main()
{
    print_sizes();
    return 0;
}
