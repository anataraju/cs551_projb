#include <iostream>
#include <cstring>
#include <vector>

#include "common.h"

using namespace std;


typedef struct proj_params{
	int iStage;
	int iClients;	// we don't know the number of clients upfront
	long long lNonce;
} proj_params_t;


enum MGR_STATES {
	NO_CLIENTS = 0,
	FIRST_CLIENT_CREATED
};

void print_usage();

class client_info
{
	// manager's info about clients
public:
	uint32_t id;
	uint32_t port;
	int csock;
	char name[81];


	client_info() { csock = -1; id = port = 0; strcpy(name, "");}
	
	client_info(int sock, uint32_t i, uint32_t p, const char *nm) {
		csock = sock; id = i; port = p;
		strcpy(name, nm);
	}
	~client_info() {}

	client_info & operator=(const client_info &c)
	{
		id = c.id; csock = c.csock; port = c.port;
		strcpy(name, c.name);
		return *this;
	}
	client_info(const client_info &c) 
	{
		(*this) = c;
	}

	int get_sock() { return csock; }
	uint32_t get_id() { return id; }


};
//int stripLastNewline(char *buffer);


class Manager {

public:
	uint32_t nonce;
	int stage;
	int listen_sock;
	int client_count;
	int state;
	int mgr_sock;

	vector<client_info> cinfo_vec;

	uint32_t listen_port;

	int fc_sock;
	uint32_t fc_port;		// first client's TCP port
	char fc_name[MAX_CLIENT_NAME_LENGTH];
	uint32_t fc_id;


// getter/setters
	int get_stage() { return stage; }
	uint32_t get_nonce() { return nonce; }
	uint32_t get_listen_port () { return listen_port; }

	// constructor?
	Manager() 
	{
		nonce = 0;
		stage = 0;
		listen_sock = -1;
		client_count = 0;
		state = NO_CLIENTS;

		listen_port = 0;

		strcpy(fc_name, "");
		fc_port = 0; fc_id = 0;
		fc_sock = 0;
		cinfo_vec.clear();
	}

	// destructor?
	~Manager() {}

	int open_tcp_listener();
	int parse_config(const char *filename);	

protected:

	void set_stage(int stg) {stage = stg;}
	void set_nonce(uint32_t n) {nonce = n;}
	void set_listen_port (int x) { listen_port = x; }
	void set_listen_sock (int x) { listen_sock = x; }
	void set_mgr_sock (int x) { mgr_sock = x; }

	void get_log_filename(char *s);
	void appendToLog(char *data);
	
	int create_client();
	int whichClient(int sockfd);
	uint32_t accept_connection();
	int send_control_info(int cliSock, const char *name);

	uint32_t parse_client_control(const char *p);
	void wait_and_exit();

	int select_loop(int cli_sock);
	int send_store_msg(int, char*);
	int send_search_msg(int, char*);
	int send_dump_msg(char *data);
	int send_end_client_msg(char *data);

	int remove_client_info(int sockfd);
	int send_exits();


};
