#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>
#include <vector>

#include "server.h"
#include "client.h"
#include "common.h"

#define LISTEN_QUEUE 20
#define MAX_LINE_SIZE 1023
#define FILE_NAME_LENGTH 256
#define MAX_CLIENTS		256
//#define OUTPUT_FILENAME	"stage1.manager.out"

using namespace std;

int connSocks[MAX_CLIENTS];
//FILE *logfile = NULL;

void print_usage()
{
	fprintf(stderr, "Usage: manager <config>\n");
}

void Manager::get_log_filename(char *fname) 
{
	sprintf(fname, "stage%d.manager.out", get_stage());
}

void Manager::appendToLog(char *data)
{
	char fname[FILE_NAME_LENGTH];
	get_log_filename(fname);

	FILE *logfile = fopen(fname, "a");
	if (NULL == logfile) {
		fprintf(stderr, "Unable to append to log\n");
		exit(0);
	}

	if (NULL == data) {
		fprintf(stderr, "Nothing to append to log\n");
	} else {
		fprintf(logfile, "%s", data);
	}
	fclose(logfile);
}

int Manager::parse_config(const char *filename)
{
	char line[MAX_LINE_SIZE+1];
	char buff[MAX_LINE_SIZE+1];
	char *pch = NULL;

	FILE * fp = fopen(filename, "r");
	if (fp == NULL) {
		fprintf(stderr, "Input file could not be opened!");
		return -1;
	}

	// fprintf(stdout, "Opened Input file for reading\n");

	memset(line, 0, sizeof(buff));
	while (fgets(line, MAX_LINE_SIZE, fp) != NULL) {
		if (line[0] == '#')
			continue;	// ignore comment line

		strncpy(buff, line, MAX_LINE_SIZE);
		pch = strtok(buff, " \n\t");

		if (strncmp(pch, "stage", strlen("stage") ) == 0) 
		{
			pch = strtok(NULL, " \n\t");
			set_stage ( atoi(pch) );
			fprintf(stdout, "read stage to be = %d\n", get_stage() );
		}
		else if (strncmp(pch, "nonce", strlen("nonce") )== 0) 
		{
			pch = strtok(NULL, " \n\t");
			set_nonce ( (uint32_t)strtoul (pch, NULL, 0) );
			fprintf(stdout, "read nonce to be = %u\n", get_nonce() );
		} 
		else if (strncmp(pch, "start_client", strlen("start_client") )== 0) 
		{
			pch = strtok(NULL, " \n\t");
			if (strnlen(pch, 82) > 80)
				die( "Max chars in client name:80\n" );

			create_client();
			
			int cli_sock = accept_connection();	// accepts a single tcp connection on listen_port

/*			if (cli_sock == 0) {
				// parent process
				int status = 0;
				waitpid(-1, &status, 0); // don't go ahead until this connection has been serviced

			} else {*/
				// service process

			send_control_info(cli_sock, pch);

				// stay alive instead
			/*	exit(0);
			}*/
			
		}
		else if (strncmp(pch, "store", strlen("store") )== 0)
		{
			pch = strtok(NULL, " \n\t");
			if (strnlen(pch, 82) > 80)
				die( "Max chars allowed in data:79\n");

			//int cli_sock = cinfo_vec.at(0).get_sock();	// first client's TCP socket
			
			fprintf(stdout, "Got First Client on Socket:%d\n", fc_sock);
			
			send_store_msg(fc_sock, pch);
		} 
		else if (strncmp(pch, "search", strlen("search") )== 0)
		{
			pch = strtok(NULL, " \n\t");
			if (strnlen(pch, 82) > 80)
				die( "Max chars allowed in data:79\n");

			//int cli_sock = cinfo_vec.at(0).get_sock();	// first client's TCP socket
			
			fprintf(stdout, "Got First Client on Socket:%d\n", fc_sock);
			
			send_search_msg(fc_sock, pch);
		} 
		else if (strncmp(pch, "dump", strlen("dump") )== 0)
		{
			pch = strtok(NULL, " \n\t");
			if (strnlen(pch, 82) > 80)
				die( "Max chars allowed in data:79\n");

			//int cli_sock = cinfo_vec.at(0).get_sock();	// first client's TCP socket
			
			//fprintf(stdout, "Got First Client on Socket:%d\n", fc_sock);
			
			send_dump_msg(pch);
		}
		else if (strncmp(pch, "end_client", strlen("end_client") )== 0)
		{
			pch = strtok(NULL, " \n\t");
			if (strnlen(pch, 82) > 80)
				die( "Max chars allowed in data:79\n");

			//int cli_sock = cinfo_vec.at(0).get_sock();	// first client's TCP socket
			
			//fprintf(stdout, "Got First Client on Socket:%d\n", fc_sock);
			
			send_end_client_msg(pch);

		}
		else
		{
			fprintf(stderr, "Unknown command encountered: %s\n", pch);
			//return -1;
		}
	}

	fclose(fp);

	wait_and_exit();

	return 0;
}

int Manager::select_loop(int cli_sock)
{

	return 0;
}

int Manager::create_client () 
{
	pid_t pid = 0;
	client_count += 1;

	if ( (pid = fork()) == 0 ) 
	{
 		close(listen_sock);

 		fprintf(stdout, "In create_client\n");

 		client c ( get_listen_port(), get_stage() );
 		// service_proc(newsockfd, p);
 		c.start();

 		// close(newsockfd);

 		exit(0);
 	}

 	if ( pid < 0 )
 		die("Error forking a new client\n");

	return 0;
}



void Manager::wait_and_exit()
{
	int status = 0;
	fprintf(stdout, "No more instructions: Asking clients to exit\n");

	send_exits();

	for (int x = 0; x < client_count; ++x)
		waitpid(-1, &status, 0);
	fprintf(stdout, "Last client exited - exiting\n");

	//close()
}

// create a tcp socket
int Manager::open_tcp_listener()
{
	struct sockaddr_in servAddr;
	socklen_t len;
	int err = 0;
	
	len = sizeof(servAddr);
	memset(&servAddr, 0, sizeof(servAddr));
	
	if ( (listen_sock = socket(AF_INET, SOCK_STREAM, 0) ) < 0 )
		die ("Unable to create socket\n");

	set_listen_sock(listen_sock);

	servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port = htons(0);

	if( ( err = bind(listen_sock, (struct sockaddr *) &servAddr, sizeof(servAddr))<0 ) ) 
        die ("Problem in bind\n");
	
	if ( (err = listen(listen_sock, LISTEN_QUEUE) ) < 0)
		die ("Problem in listen\n");

	if (getsockname(listen_sock, (struct sockaddr *)&servAddr, &len) < 0)
		die("Getsockname failed\n");
	else {
		fprintf(stdout, "manager port %d\n", ntohs(servAddr.sin_port)); 
	}

	set_listen_port(ntohs(servAddr.sin_port));
	
	return 0; 
}

// returns zero based index
int Manager::whichClient(int sockfd)
{
	int ret = 0;
	if (sockfd <= 0)
		return 0;
	for(int i = 0; i < MAX_CLIENTS; ++i) {
		if (sockfd == connSocks[i]) {
			ret = i;
		}

	}
	return ret;
}

uint32_t Manager::accept_connection() {
	
	socklen_t clilen = 0;
	int newsockfd = 0;

	struct sockaddr_in cliAddr;

	memset(connSocks, 0, sizeof(int) * MAX_CLIENTS);

	// fprintf(stdout, "Blocked before accept\n");
	clilen = sizeof(cliAddr);
 	newsockfd = accept(listen_sock, (struct sockaddr *) &cliAddr, &clilen);	

 	if (newsockfd < 0) {
 		fprintf(stderr, "Error on accept\n");
 		return 0;
 	}

 	connSocks[client_count] = newsockfd;

 	return newsockfd;

 	/*if ( (pid = fork() ) == 0 ) {
 		close(listen_sock);

 		// service_proc(newsockfd, p);

 		close(newsockfd);
 		exit(0);
 		return newsockfd;
    } else if (pid < 0) {
    	die ("Could not fork service process!");
    }

	close (newsockfd);
	return 0;*/
}

int Manager::send_control_info(int cliSock, const char *name) 
{
	char buffer[1024];
	char wb[1024];	// writebuffer
	ssize_t nbytes = 0;
	fprintf(stdout, "Service process started: %d\n", getpid());
	memset(buffer, 0, 1024);
 	
 	int client_num = whichClient(cliSock) + 1;

 	strcpy(buffer, "");
 	if (0 == fc_port) {	
 		sprintf(buffer, "%u\n%s\n%u\n%s", get_nonce(), name, fc_port, name);
 		strcpy(fc_name, name);
 		fprintf(stdout, "Set first client name: %s\n", fc_name);

 	} else {
 		sprintf(buffer, "%u\n%s\n%u\n%s", get_nonce(), name, fc_port, fc_name);
 	}

 	// fprintf(stdout, "HEREREREREEEEEEEEEEEEEER: %s\n", buffer);
 	fprintf(stdout, "-----------------------------------------\n");
 	nbytes = write(cliSock, buffer, strlen(buffer));
 	if (nbytes < 0) {
 		fprintf(stderr, "Write to client failed\n");
 		return -1;
 	}

 	memset(buffer, 0, 1023);
 	// fprintf(stdout, "Blocked on read\n");
 	nbytes = read(cliSock, buffer, 1023);
 	if (nbytes < 0) {
 		fprintf(stderr, "Read from client failed\n");
 		return -1;
 	}

 	//stripLastNewline(buffer);
 	uint32_t port = parse_client_control(buffer);
 	if (0 == fc_port) {		// only first client
 		fc_port = port;
 		fprintf(stdout, "Set first client port: %u\n", fc_port);
 		fc_sock = cliSock;
 	}
 	sprintf(wb, "client %d says:%s\n", client_num, buffer);
 	appendToLog(wb);

 	char buf_name[81]; strcpy(buf_name, name);
 	cinfo_vec.push_back(client_info(cliSock, nonce_name_hash(nonce, buf_name), port, name));
	
	return 0;
}

uint32_t Manager::parse_client_control(const char *p)
{
	char buffer[1024];
	char *pch = NULL;
	strcpy(buffer, p);

	pch = strtok(buffer, "\n");
//	uint32_t id = (uint32_t)strtoul(pch, NULL, 0);

	pch = strtok(NULL, "\n");
	uint32_t port = (uint32_t) strtoul(pch, NULL, 0);

	return port;
}

int main(int argc, char *argv[]) {
	//struct proj_params params;
	
	if (argc < 2) 
	{
		print_usage();
		return 0;
	}

	// memset(&params, 0, sizeof(proj_params_t));

	Manager mgr;

	//open the file to write log
	/*FILE *logfile = fopen(OUTPUT_FILENAME, "w");
	if (logfile == NULL) {
		fprintf(stderr, "Unable to open log file to write\n");
		return 0;
	}*/

	// dynamically assign a port for tcp listen
	if ( mgr.open_tcp_listener() < 0) {
		fprintf(stderr, "open_tcp_listener failed\n");
		return 0;
	}

	/*fprintf(logfile, "manager port: %d\n", portNum);
	fclose(logfile);*/

	if (mgr.parse_config(argv[1]) < 0) {
		fprintf(stderr, "Parse error, exiting!");
		return -1;
	}

	return 0;
}

int Manager::send_store_msg(int sockfd, char *data)
{
	char buffer[1024];
	ssize_t nbytes = 0;
	memset(buffer, 0, 1024);
 	
 	strcpy(buffer, "store ");
 	strcat(buffer, data);
 	
 	// fprintf(stdout, "HEREREREREEEEEEEEEEEEEER: %s\n", buffer);
 	fprintf(stdout, "Manager:--------------STORE--------%s-----------\n", data);
 	nbytes = send(sockfd, buffer, strlen(buffer), 0);
 	if (nbytes < 0) {
 		fprintf(stderr, "Write to client failed\n");
 		return -1;
 	}

 	memset(buffer, 0, 1023);
 	// fprintf(stdout, "Blocked on read\n");
 	nbytes = read(sockfd, buffer, 1023);
 	if (nbytes < 0) {
 		fprintf(stderr, "Read from client failed\n");
 		return -1;
 	}

 	if (strncmp(buffer, "OK\n", strlen("OK\n")) == 0)
 	{
 		fprintf(stdout, "Manager: Successfully stored data %s\n", data);
 	} 
 	else
 	{
 		fprintf(stdout, "Manager: Could not store data successfully: %s\n", data);	
 	}

	return 0;
}

int Manager::send_search_msg(int sockfd, char *data)
{
	char buffer[1024];
	ssize_t nbytes = 0;
	memset(buffer, 0, 1024);
 	
 	strcpy(buffer, "search ");
 	strcat(buffer, data);
 	
 	// fprintf(stdout, "HEREREREREEEEEEEEEEEEEER: %s\n", buffer);
 	fprintf(stdout, "Manager:--------------SEARCH--------%s-----------\n", data);
 	nbytes = write(sockfd, buffer, strlen(buffer));
 	if (nbytes < 0) {
 		fprintf(stderr, "Write to client failed\n");
 		return -1;
 	}

 	memset(buffer, 0, 1023);
 	// fprintf(stdout, "Blocked on read\n");
 	nbytes = read(sockfd, buffer, 1023);
 	if (nbytes < 0) {
 		fprintf(stderr, "Read from client failed\n");
 		return -1;
 	}

 	if (strncmp(buffer, "OK\n", strlen("OK\n")) == 0)
 	{
 		fprintf(stdout, "Manager: Successfully searched data %s\n", data);
 	} 
 	else
 	{
 		fprintf(stdout, "Manager: The command did not complete: search %s\n", data);	
 	}
 	return 0;
}

int Manager::send_dump_msg(char *data)
{
	char buffer[1024];
	ssize_t nbytes = 0;
	memset(buffer, 0, 1024);
 	
 	strcpy(buffer, "dump ");
 	//strcat(buffer, data);

 	int sockfd;

 	// get socket based on client name
 	for (size_t i = 0; i < cinfo_vec.size(); ++i) {
 		if (strncmp(data, cinfo_vec.at(i).name, strlen(data) )== 0)
 			sockfd = cinfo_vec.at(i).csock;
 	}

 	fprintf(stdout, "Got Client %s on Socket:%d\n", data, sockfd);
 	
 	// fprintf(stdout, "HEREREREREEEEEEEEEEEEEER: %s\n", buffer);
 	fprintf(stdout, "Manager:--------------DUMP--------%s-----------\n", data);
 	nbytes = write(sockfd, buffer, strlen(buffer));
 	if (nbytes < 0) {
 		fprintf(stderr, "Write to client failed\n");
 		return -1;
 	}

 	memset(buffer, 0, 1023);
 	// fprintf(stdout, "Blocked on read\n");
 	nbytes = read(sockfd, buffer, 1023);
 	if (nbytes < 0) {
 		fprintf(stderr, "Read from client failed\n");
 		return -1;
 	}

 	if (strncmp(buffer, "OK\n", strlen("OK\n")) == 0)
 	{
 		fprintf(stdout, "Manager: Dumped data for %s\n", data);
 	} 
 	else
 	{
 		fprintf(stdout, "Manager: The command did not complete: dump %s\n", data);	
 	}
 	return 0;	
}

int Manager::send_end_client_msg(char *data)
{
	char buffer[1024];
	ssize_t nbytes = 0;
	memset(buffer, 0, 1024);
 	
 	strcpy(buffer, "end_client ");
 	strcat(buffer, data);

 	int sockfd;

 	// get socket based on client name
 	for (size_t i = 0; i < cinfo_vec.size(); ++i) {
 		if (strncmp(data, cinfo_vec.at(i).name, strlen(data) )== 0)
 			sockfd = cinfo_vec.at(i).csock;
 	}

 	fprintf(stdout, "Got Client %s on Socket:%d\n", data, sockfd);
 	
 	// fprintf(stdout, "HEREREREREEEEEEEEEEEEEER: %s\n", buffer);
 	fprintf(stdout, "Manager:--------------END_CLIENT--------%s-----------\n", data);
 	nbytes = write(sockfd, buffer, strlen(buffer));
 	if (nbytes < 0) {
 		fprintf(stderr, "Write to client failed\n");
 		return -1;
 	}

 	memset(buffer, 0, 1023);
 	// fprintf(stdout, "Blocked on read\n");
 	nbytes = read(sockfd, buffer, 1023);
 	if (nbytes < 0) {
 		fprintf(stderr, "Read from client failed\n");
 		return -1;
 	}

 	if (strncmp(buffer, "OK\n", strlen("OK\n")) == 0)
 	{
 		fprintf(stdout, "Manager: end_client %s done\n", data);
 	} 
 	else
 	{
 		fprintf(stdout, "Manager: The command did not complete: end_client %s\n", data);	
 	}

 	remove_client_info(sockfd);

 	return 0;
}

int Manager::send_exits()
{
	int sockfd;
	char buffer[1024];
	ssize_t nbytes = 0;
	memset(buffer, 0, 1024);
 	
 	strcpy(buffer, "exit");

 	// get socket based on client name
 	for (size_t i = 0; i < cinfo_vec.size(); ++i)
 	{
 		sockfd = cinfo_vec.at(i).csock;

 		fprintf(stdout, "Manager:--------------EXIT-------%s-----------\n", cinfo_vec.at(i).name);
	 	nbytes = write(sockfd, buffer, strlen(buffer));
	 	if (nbytes < 0) {
	 		fprintf(stderr, "Write to client failed\n");
	 		return -1;
	 	}

	 	/*memset(buffer, 0, 1023);
	 	// fprintf(stdout, "Blocked on read\n");
	 	nbytes = read(sockfd, buffer, 1023);
	 	if (nbytes < 0) {
	 		fprintf(stderr, "Read from client failed\n");
	 		return -1;
	 	}

	 	if (strncmp(buffer, "OK\n", strlen("OK\n")) == 0)
	 		fprintf(stdout, "Manager: exit %s done\n", cinfo_vec.at(i).name);
	 	else
	 		fprintf(stdout, "Manager: The command did not complete: EXIT %s\n", cinfo_vec.at(i).name);*/	
 	}
 	return 0;
}

int Manager::remove_client_info(int sockfd)
{
	vector<client_info>::iterator p_iter = cinfo_vec.begin();
	int exit_flag = 0;

	if (sockfd == fc_sock) 
	{
		if (cinfo_vec.size() > 1) // assign next client as first client
		{
			fc_sock = cinfo_vec.at(1).csock;
			fc_port = cinfo_vec.at(1).port;
			fc_id = cinfo_vec.at(1).id;
			strcpy(fc_name, cinfo_vec.at(1).name);
		}
		else
		{
			fprintf(stderr, "Manger: We are removing first client!!!\n");
			fprintf(stderr, "There is no new client that has joined: Exiting\n");

			exit_flag = 1;
		}
	}

	while (p_iter != cinfo_vec.end())
	{
		if (p_iter->csock == sockfd)
		{
			// close(sockfd);

			cinfo_vec.erase(p_iter);
			break;
		}
		p_iter++;
	}

	if (exit_flag)
		exit(0);

	return 0;
}