#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>

#include "client.h"

using namespace std;

void client::process_tcp_store_cmd(char *S)
{
	uint8_t buffer[1024];
	uint32_t SL = strlen(S);
	struct sockaddr_in cliaddr;
	if (SL == 0)
		die("Nothing to be stored - Problem with input!");

	uint32_t datahash = nonce_name_hash(nonce, S);
    fprintf(stdout, "Data %s : nonce %d : hash: 0x%x\n", S, nonce, datahash);

    int that_condition = (hashval < suc_id && hashval < pre_id && 
                       ( (datahash > pre_id) || datahash <= hashval ) );

	// first client stores data; or if it's the only client in ring
	//if (suc_id == hashval || (datahash > pre_id && datahash <= hashval) || that_condition)
    if (suc_id == hashval || inRightClosedRange(datahash, pre_id, hashval) || that_condition)
	{
		data_vec.push_back(data(S, datahash));
		fprintf(stdout, "Data %s with hash 0x%x, stored in 0x%x: %s\n",
				S, datahash, hashval, name);

        fprintf(fptr, "add %s with hash 0x%x to node 0x%x\n", S, datahash, hashval);
	}
	else
	{
		uint32_t tmp_port = suc_port, tmp_id = suc_id; // lets assume successor, but if we have finger table lets use it
        if (get_stage() >=4) {
            lookup_finger_table(datahash, &tmp_id, &tmp_port);
        }

        int found = 0;
		int looped = 0;
		stores_r_t sr;
		
		stores_q_t sq; sq.di = datahash; sq.ni = tmp_id;
		while (1) {	// I'm the first client, I should never be asked

			send_stores_q(tmp_port, &sq);
			recv_udp_data(buffer, sizeof(buffer), &cliaddr);
			rcv_stores_r(buffer, &sr);
			if (sr.ni == sr.ri) {
				found = 1;
				break;
			}

			// prepare next stores-q
			sq.ni = sr.ri; tmp_port = sr.rp;
			if (sq.ni == suc_id)
				looped++;

			if (looped > 1)
			{
				data_vec.push_back(data(S, datahash));
				fprintf(stdout, "Data %s with hash 0x%x, stored in 0x%x: %s\n",
						S, datahash, hashval, name);

                fprintf(fptr, "add %s with hash 0x%x to node 0x%x\n", S, datahash, hashval);

				break;
			}
		}
		if (found)
		{
			store_q_t tq; tq.ni = sr.ri; tq.SL = SL; strcpy(tq.S, S);
			fprintf(stdout, "%s: Sending data %s to node:0x%x\n",
					name, S, tq.ni);
			store_r_t tr;
			send_store_q( sr.rp, &tq);
			recv_udp_data(buffer, sizeof(buffer), &cliaddr);
			rcv_store_r(buffer, &tr);

			if (tq.ni == tr.ni && tq.SL == tr.SL &&
					(strcmp(tq.S, tr.S)==0))
			{
				if (tr.R == 0)
					fprintf(stdout, "store FAILED!\n");
				else if (tr.R == 1) {
					fprintf(stdout, "store SUCCESS!\n");
                    fprintf(fptr, "add %s with hash 0x%x to node 0x%x\n", S, datahash, tr.ni);
                } else if (tr.R == 2)
					fprintf(stdout, "data already PRESENT!\n");
			}
		}
	}
	send_ok();
}

int client::is_hash_present(uint32_t hash)
{
	for (size_t i =0; i < data_vec.size(); ++i)
	{
		if (data_vec.at(i).dhash == hash)
			return 1;
	}
	return 0;
}

void client::process_tcp_search_cmd(char *S)
{
	uint8_t buffer[1024];
	struct sockaddr_in cliaddr;
	uint32_t SL = strlen(S);
	if (SL == 0)
		die("Nothing to be searched - Problem with input!");

	uint32_t datahash = nonce_name_hash(nonce, S);

    int that_condition = (hashval < suc_id && hashval < pre_id && 
                       ( (datahash > pre_id) || datahash <= hashval ) );

	// first client stores data; or if it's the only client in ring
	if (suc_id == hashval || (datahash > pre_id && datahash <= hashval) || that_condition)
	{
		if (is_hash_present(datahash)) {
			fprintf(stdout, "Search Data %s with hash 0x%x, FOUND in 0x%x: %s\n",
				S, datahash, hashval, name);
            
            fprintf(fptr, "search %s to node 0x%x, key PRESENT\n", S, hashval);
		} else {
			fprintf(stdout, "Search Data %s with hash 0x%x, NOT FOUND in 0x%x: %s\n",
				S, datahash, hashval, name);
            
            fprintf(fptr, "search %s to node 0x%x, key ABSENT\n", S, hashval);
		}
	}
	else
	{
		uint32_t tmp_port = suc_port, tmp_id = suc_id; // lets assume successor, but if we have finger table lets use it
        if (get_stage() >=4) {
            lookup_finger_table(datahash, &tmp_id, &tmp_port);
        }


        int found = 0;
		int looped = 0;
		stores_r_t sr;
		
		stores_q_t sq; sq.di = datahash; sq.ni = tmp_id;
		while (1) {	// I'm the first client, I should never be asked

			send_stores_q(tmp_port, &sq);
			recv_udp_data(buffer, sizeof(buffer), &cliaddr);
			rcv_stores_r(buffer, &sr);
			if (sr.ni == sr.ri) {
				found = 1;
				break;
			}

			// prepare next stores-q
			sq.ni = sr.ri; tmp_port = sr.rp;
			if (sq.ni == suc_id)
				looped++;

			if (looped > 1)
			{
				if (is_hash_present(datahash)) {
					fprintf(stdout, "Search Data %s with hash 0x%x, FOUND in 0x%x\n",
						S, datahash, hashval);

                    fprintf(fptr, "search %s to node 0x%x, key PRESENT\n", S, hashval);
				}
			}
		}
		if (found)
		{
			if (sq.ni == sr.ni && sq.di == sr.di)
			{
				if (sr.has == 0) {
					fprintf(stdout, "search FAILED!\n");
                    fprintf(fptr, "search %s to node 0x%x, key ABSENT\n", S, sr.ni);
                }
				else if (sr.has == 1) {
					fprintf(stdout, "search SUCCESS!\n");
					fprintf(stdout, "Search Data %s with hash 0x%x, FOUND in 0x%x\n",
											S, datahash, sr.ri);
                    fprintf(fptr, "search %s to node 0x%x, key PRESENT\n", S, sr.ni);
				}
			}
		}
	}
	send_ok();
}

void client::process_tcp_leaving_cmd(char *S)
{
    uint8_t buffer[1024];
    struct sockaddr_in cliaddr;
    uint32_t SL = strlen(S);
    
    READY_TO_EXIT1 = 0;
    READY_TO_EXIT2 = 0;

    if (SL == 0)
        die("Nothing to be done - Problem with input!");

    fprintf(stdout, "%s I'm supposed to leave now!!\n", name);

    uint32_t tmp_id = suc_id; uint32_t tmp_port = suc_port;
    
    // find the sucessor's successor and corresponding port, push it to vector
    // send_leaving_q to all of them - note you have skipped your successor
    while (tmp_id != hashval) 
    {
        send_sucpred_q(SUC_Q, tmp_id, tmp_port);
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_sucpred_r(buffer, &tmp_id, &tmp_port);

        vec_ports.push_back(tmp_port); vec_ids.push_back(tmp_id);
    }

    lv_r lvreply;
    for (int i = 0; i < (int)vec_ports.size(); ++i) {
        
        send_leaving_q(vec_ids.at(i), vec_ports.at(i));
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_sucpred_q(buffer);
        send_sucpred_r(SUC_Q, ntohs(cliaddr.sin_port));
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_leaving_r(buffer, &lvreply);    // doesn't matter what we receive
    }

    // now take care of successor
    send_leaving_q(suc_id, suc_port);

    nd_q ndq;
    int flag = 0;

    while(1) {
        recv_udp_data(buffer, 1024, &cliaddr);
        rcv_next_data_q(buffer, &ndq);
        flag = send_next_data_r(&ndq);
        if (flag)
            break;
    }

    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_q(buffer);
    send_sucpred_r(PRE_Q, ntohs(cliaddr.sin_port)); 
    
    recv_udp_data(buffer, 1024, &cliaddr); 
    rcv_leaving_r(buffer, &lvreply);  

    // fprintf(stdout, "!!!!!!!Client %s- Exiting!!!!!!\n", name);

    send_ok();

    cleanup_and_exit();
}

void client::send_sucpred_q(int type, uint32_t dest_id, uint32_t dest_port)
{
    sucq_t succ_q;
    succ_q.ni = dest_id;
    succ_q.type = type;
    uint8_t *buffer = (uint8_t *) malloc(sizeof(sucq_t));

    memcpy((void*)buffer, &succ_q, sizeof(succ_q));

    int sockfd, n;
    struct sockaddr_in serverAddr;

    if (SUC_Q == type) {
        fprintf(stdout, "I'm 0x%x: Sending suc_q to dest_id 0x%x on port %d\n", hashval, dest_id,  dest_port);
        fprintf(fptr, "successor-q sent (0x%x)\n", dest_id);
    } else  {
        fprintf(stdout, "I'm 0x%x: Sending pre_q to dest_id 0x%x on port %d\n", hashval, dest_id,  dest_port);
        fprintf(fptr, "predecessor-q sent (0x%x)\n", dest_id);
    }

    sockfd = get_listen_sock();

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    serverAddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(succ_q), 0, (struct sockaddr *)&serverAddr,sizeof(serverAddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: suc_q message!!\n");
    }

    free(buffer);
}

void client::send_sucpred_r(int type, uint32_t dest_port)
{
    sucr_t succ_r;
    succ_r.ni = hashval;

    if (type == SUC_Q)
    {
        succ_r.type = SUC_R;
        succ_r.si = suc_id;
        succ_r.sp = suc_port;
    }
    else
    {
        succ_r.type = PRE_R;
        succ_r.si = pre_id;
        succ_r.sp = pre_port;
    }

    uint8_t *buffer = (uint8_t *) malloc(sizeof(sucr_t));

    memcpy((void*)buffer, &succ_r, sizeof(succ_r));

    int sockfd, n;
    struct sockaddr_in myaddr;

    if (type == SUC_Q) {
        fprintf(stdout, "I'm 0x%x: Sending suc_r to dest on port %d\n", hashval, dest_port);
        fprintf(fptr, "successor-r sent (0x%x 0x%x %d)\n", succ_r.ni, succ_r.si, succ_r.sp);

    } else {
        fprintf(stdout, "I'm 0x%x: Sending pre_r to dest on port %d\n", hashval, dest_port);
        fprintf(fptr, "predecessor-r sent (0x%x 0x%x %d)\n", succ_r.ni, succ_r.si, succ_r.sp);
    }

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(succ_r), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: suc_q message!!\n");
    }

    free(buffer);
}

void client::rcv_sucpred_r(uint8_t *rcvbuf /*input*/, uint32_t *ri, uint32_t *rp)
{
    sucr_t *precv = (sucr_t *) rcvbuf;

    if (precv->type == SUC_R)
    {
        fprintf(stdout, "I'm 0x%x: Received successor-r 0x%x, port %d\n", hashval, precv->si,  precv->sp);
        fprintf(fptr, "successor-r received (0x%x 0x%x %d)\n", precv->ni, precv->si, precv->sp);
        *ri = precv->si;
        *rp = precv->sp;
        if (log_all)
            printRaw(fptr, rcvbuf, sizeof(sucr_t));
    }
    else if (precv->type == PRE_R)
    {
        fprintf(stdout, "I'm 0x%x: Received predecessor-r 0x%x, port %d\n", hashval, precv->si,  precv->sp);
        fprintf(fptr, "predecessor-r received (0x%x 0x%x %d)\n", precv->ni, precv->si, precv->sp);
        *ri = precv->si;
        *rp = precv->sp;
        if (log_all)
            printRaw(fptr, rcvbuf, sizeof(sucr_t));
    }
    else
    {
        die("Did not receive expected reply from node!!!!\n");
    }    
}

void client::rcv_sucpred_q(uint8_t * rcvbuf /*input*/)
{
    // struct sockaddr_in client;
    // int sockfd = get_listen_sock();

    sucr_t *precv = (sucr_t *) rcvbuf;
    if (hashval == precv->ni)
    {
        if (precv->type == SUC_Q) {
            fprintf(stdout, "I'm 0x%x: Received successor-q 0x%x\n", hashval, precv->ni);
            fprintf(fptr, "successor-q received (0x%x)\n", precv->ni);
            if (log_all)
                printRaw(fptr, rcvbuf, sizeof(sucr_t));
        }
        else {
            fprintf(stdout, "I'm 0x%x: Received predecessor-q 0x%x\n", hashval, precv->ni);    
            fprintf(fptr, "predecessor-q received (0x%x)\n", precv->ni);    
            if (log_all)
                printRaw(fptr, rcvbuf, sizeof(sucr_t));
        }
    }
    else
    {
        fprintf(stderr, "%s: \n", name);
        die("Did not receive expected reply from node!!!!\n");
    }
}

void client::recv_udp_data(uint8_t *buffer, size_t bufflen, struct sockaddr_in *cliaddr)
{
    socklen_t len = sizeof(struct sockaddr_in);

    memset(cliaddr, 0, len);
    memset(buffer, 0, bufflen);

    int n = recvfrom(my_listen_sock, buffer, bufflen, 0, (struct sockaddr *)cliaddr, &len);
    if (n < 0) {
        die ("Error receiving data\n");
    }
}

void client::recv_tcp_data(uint8_t *buffer, size_t bufflen)
{
    memset(buffer, 0, bufflen);
    int tcp_sock = get_mgr_sock();

    int n = read(tcp_sock, buffer, bufflen);
    if (n < 0) {
        die ("Client: Error receiving tcp data\n");
    }
}

// Refer Figure 6: Update finger table pseudo code
int client::rcv_upd_q(uint8_t *rcvbuf, updr_t *reply)
{
    updq_t *precv = (updq_t *) rcvbuf;
    if (hashval == precv->ni) 
    {
        fprintf(stdout, "I'm 0x%x: Received update-q si: 0x%x sp: %d row(i): %d\n",
                precv->ni, precv->si, precv->sp, precv->i);

        fprintf(fptr, "update-q received (0x%x 0x%x %d %d)\n", precv->ni, precv->si, precv->sp, precv->i);
        if (log_all)
            printRaw(fptr, rcvbuf, sizeof(updq_t));

        reply->type = UPD_R;
        reply->R = 0;   //indicate success 1/failure 0
        reply->ni = precv->ni;
        int i = reply->i = precv->i; 
        reply->si = precv->si;
        reply->sp = precv->sp;

        if (get_stage() >= 4 ) 
        {         
            if (i <= MAX_FT_ENTRIES) 
            {
                if (i == 0) 
                {
                    pre_id = ft.at(0).id = precv->si;
                    pre_port = ft.at(0).id_port = precv->sp;
                    fprintf(stdout, "Client %s: I updated my predecessor\n", name);
                    reply->R = 1;                    
                    return 0;
                }   

                if ( inLeftClosedRange( precv->si, ft.at(i).start, ft.at(i).id ) ) 
                {
                    ft.at(i).id = precv->si;
                    ft.at(i).id_port = precv->sp;
                    reply->R = 1;
                    if (i == 1) 
                    {
                        suc_id = precv->si;
                        suc_port = precv->sp;
                        fprintf(stdout, "Client %s: I updated my successor\n", name);
                    } 
                } 
            } 
            else 
            {
                fprintf(stderr, "Client %s: ", name);
                die ("Received Update for a non-existing ft entry\n");
            }
        } 
        else 
        {
            if (precv->i == 0) {
                pre_id = precv->si;
                pre_port = precv->sp;
                reply->R = 1;
            } else if (precv->i == 1) {
                suc_id = precv->si;
                suc_port = precv->sp;
                reply->R = 1;
            } else {
                die("Finger table cannot be used yet!\n");
            }
            return 0;
        }
    } 
    else 
    {
        fprintf(stderr, "%s:", name);
        die("This query was not meant for me!!!!\n");
    }
    return 0;
}

void client::send_upd_r(uint32_t dest_port, updr_t *reply)
{
    uint8_t *buffer = (uint8_t *) malloc(sizeof(updr_t));

    memcpy((void*)buffer, reply, sizeof(updr_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending upd_r to dest on port %d\n", hashval, dest_port);
    // fprintf(fptr, "update-r sent (0x%x %d 0x%x %d %d)\n", reply->ni, reply->R, reply->si, reply->sp, reply->i);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(updr_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: update-r message!!\n");
    }

    free(buffer);
}

void client::send_upd_q(uint32_t dest_port, updq_t *reply)
{
    uint8_t *buffer = (uint8_t *) malloc(sizeof(updq_t));

    memcpy((void*)buffer, reply, sizeof(updq_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending upd_q(0x%x, 0x%x, %d, %d) to dest on port %d\n", 
        hashval, reply->ni, reply->si, reply->sp, reply->i, dest_port);

    fprintf(fptr, "update-q sent (0x%x 0x%x %d %d)\n", reply->ni, reply->si, reply->sp, reply->i);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(updq_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: update-q message!!\n");
    }

    free(buffer);
}

void client::rcv_upd_r(uint8_t *rcvbuf, int *status)
{
    updr_t *precv = (updr_t *) rcvbuf;


    fprintf(stdout, "I'm 0x%x: Received update-r si: 0x%x sp: %d i: %d status: %d\n",
            hashval, precv->si, precv->sp, precv->i, precv->R);

/*    fprintf(fptr, "update-r received (0x%x 0x%x %d %d %d)\n", precv->ni, precv->si, precv->sp, precv->i, precv->R);
    if (log_all)
        printRaw(fptr, rcvbuf, sizeof(updr_t));*/
    
    *status = precv->R;
}


void client::rcv_stores_q(uint8_t *buf, stores_q_t * pq)
{
    //stores_q_t *p = (stores_q_t *) buf;

    memcpy(pq, buf, sizeof (stores_q_t) );

    fprintf(stdout, "I'm 0x%x: Received stores-q (0x%x, 0x%x)\n", hashval, pq->ni, pq->di);

    fprintf(fptr, "stores-q received (0x%x 0x%x)\n", pq->ni, pq->di);
    if (log_all)
        printRaw(fptr, buf, sizeof(stores_q_t));
}

void client::rcv_stores_r(uint8_t *buf, stores_r_t * pr)
{
    //stores_r_t *p = (stores_r_t *) buf;
    memcpy(pr, buf, sizeof (stores_r_t) );


    fprintf(stdout, "I'm 0x%x: Received stores-r (0x%x, 0x%x, 0x%x, %d, %d)\n",
            hashval, pr->ni, pr->di, pr->ri, pr->rp, pr->has);
    
    fprintf(fptr, "stores-r received (0x%x 0x%x 0x%x %d %d)\n", pr->ni, pr->di, pr->ri, pr->rp, pr->has);
    if (log_all)
        printRaw(fptr, buf, sizeof(stores_r_t));
    //*status = pr->R;
}

void client::rcv_store_q(uint8_t *buf, store_q_t * pq)
{
    //pq = (store_q_t *) buf;
    memcpy(pq, buf, sizeof (store_q_t) );

    fprintf(stdout, "I'm 0x%x: Received store-q (0x%x, %d, %s)\n",
            hashval, pq->ni, pq->SL, pq->S);

    fprintf(fptr, "store-q received (0x%x %d %s)\n", pq->ni, pq->SL, pq->S);
    if (log_all) 
        printRaw(fptr, buf, sizeof(stores_q_t), pq->SL);

    //*status = precv->R;
}

void client::rcv_store_r(uint8_t *buf, store_r_t * pr)
{
    //pr = (store_r_t *) buf;
    memcpy(pr, buf, sizeof (store_r_t) );

    fprintf(stdout, "I'm 0x%x: Received store-r ni: 0x%x R: %d SL: %d status: %s\n",
            hashval, pr->ni, pr->R, pr->SL, pr->S);
    fprintf(fptr, "store-r received (0x%x %d %d %s)\n",  pr->ni, pr->R, pr->SL, pr->S);

    if (log_all)
        printRaw(fptr, buf, sizeof(store_r_t), pr->SL);
    //*status = precv->R;
}


void client::send_stores_q(uint32_t dest_port, stores_q_t * pq)
{
    uint8_t *buffer = (uint8_t *) malloc(sizeof(stores_q_t));
    pq->type = STS_Q;

    memcpy((void*)buffer, pq, sizeof(stores_q_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending stores_q(0x%x, 0x%x) to dest on port %d\n",
        hashval, pq->ni, pq->di, dest_port);
    fprintf(fptr, "stores-q sent (0x%x 0x%x)\n", pq->ni, pq->di);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(stores_q_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: stores_q message!!\n");
    }

    free(buffer);
}

void client::send_stores_r(uint32_t dest_port, stores_r_t * pr)
{
    uint8_t *buffer = (uint8_t *) malloc(sizeof(stores_r_t));
    pr->type = STS_R;

    memcpy((void*)buffer, pr, sizeof(stores_r_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending stores_r(0x%x, 0x%x, 0x%x, %d, %d) to dest on port %d\n",
        hashval, pr->ni, pr->di, pr->ri, pr->rp, pr->has, dest_port);

    fprintf(fptr, "stores-r sent (0x%x 0x%x 0x%x %d %d)\n", pr->ni, pr->di, pr->ri, pr->rp, pr->has);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(stores_r_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: stores_r message!!\n");
    }

    free(buffer);
}

void client::send_store_q(uint32_t dest_port, store_q_t * pq)
{
    uint8_t *buffer = (uint8_t *) malloc(sizeof(store_q_t));
    pq->type = STO_Q;

    memcpy((void*)buffer, pq, sizeof(store_q_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending store_q(0x%x, %d, %s) to dest on port %d\n",
        hashval, pq->ni, pq->SL, pq->S, dest_port);

    fprintf(fptr, "store-q sent (0x%x %d %s)\n", pq->ni, pq->SL, pq->S);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(store_q_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_q message!!\n");
    }

    free(buffer);
}

void client::send_store_r(uint32_t dest_port, store_r_t * pr)
{
    pr->type = STO_R;
    uint8_t *buffer = (uint8_t *) malloc(sizeof(store_r_t));

    memcpy((void*)buffer, pr, sizeof(store_r_t));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending store_r(0x%x, %d, %d, %s) to dest on port %d\n",
        hashval, pr->ni, pr->R, pr->SL, pr->S, dest_port);

    fprintf(fptr, "store-r sent (0x%x %d %d %s)\n", pr->ni, pr->R, pr->SL, pr->S);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(store_r_t), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_r message!!\n");
    }

    free(buffer);
}

void client::send_leaving_q(uint32_t to_id, uint32_t to_port) 
{

    lv_q q; q.type = LEV_Q; q.ni = to_id; q.di = hashval;

    uint8_t *buffer = (uint8_t *) malloc(sizeof(lv_q));

    memcpy((void*)buffer, &q, sizeof(lv_q));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending leaving-q(0x%x 0x%x) to dest on port %d\n",
        hashval, q.ni, q.di, to_port);

    fprintf(fptr, "leaving-q sent (0x%x 0x%x)", q.ni, q.di);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(to_port);

    n = sendto(sockfd, buffer, sizeof(lv_q), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_r message!!\n");
    }

    free(buffer);
}

void client::rcv_leaving_q(uint8_t *buf, lv_q *pq)
{
    memcpy(pq, buf, sizeof(lv_q));
    fprintf(stdout, "client %s: Received leaving-q(0x%x, 0x%x)\n", name, pq->ni, pq->di);

    fprintf(fptr, "leaving-q received (0x%x 0x%x)\n", pq->ni, pq->di);
    if (log_all)
        printRaw(fptr, buf, sizeof(lv_q));
}

void client::process_predecessor_leaving()
{
    struct sockaddr_in cliaddr; uint8_t buffer[1024];
    uint32_t new_pred = 0, new_pred_port = 0;

    // update my finger table
    for (int i = 1; i <= MAX_FT_ENTRIES; ++i)
        if (ft.at(i).id == pre_id) {
            ft.at(i).id = hashval; ft.at(i).id_port = my_udp_port;   
        }

    // get his predecessor

    send_sucpred_q(PRE_Q, pre_id, pre_port); // query the first client
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &new_pred, &new_pred_port);

    // update my predecessor pointers

    fprintf(stdout, "%s: HEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHE\n", name);

    uint32_t old_pre_port = pre_port;
    uint32_t old_pre_id = pre_id;
    

    ft.at(0).id = pre_id = new_pred;
    ft.at(0).id_port = pre_port = new_pred_port;

    send_leaving_r(old_pre_id, old_pre_port);

    // reset the flag, if others are trying to leave, they'll reuse it
    RECEIVED_ALL_DATA = 0;
}

void client::process_random_leaving(uint32_t id, uint32_t port)
{
    struct sockaddr_in cliaddr; uint8_t buffer[1024];
    // id, port belong to leaving node

    // first, get it's successor
    uint32_t his_successor, his_successor_port;
    send_sucpred_q(SUC_Q, id, port);
    recv_udp_data(buffer, 1024, &cliaddr);
    rcv_sucpred_r(buffer, &his_successor, &his_successor_port);

    for (int i = 1; i <= MAX_FT_ENTRIES; ++i) 
    {
        if (ft.at(i).id == id) // update any entries that correspond to him with his successor
        {
            ft.at(i).id = his_successor;
            ft.at(i).id_port = his_successor_port;

            if (1 == i)
            {
                suc_id = his_successor;
                suc_port = his_successor_port;
            }
        }
    }

    send_leaving_r(id, port);
}

void client::send_leaving_r(uint32_t dest_id, int dest_port) 
{
    lv_r r; r.type = LEV_R; r.ni = dest_id;

    uint8_t *buffer = (uint8_t *) malloc(sizeof(lv_r));

    memcpy((void*)buffer, &r, sizeof(lv_r));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending leaving-r(0x%x) to dest on port %d\n",
        hashval, r.ni, dest_port);

    fprintf(fptr, "leaving-r sent (0x%x)\n", r.ni);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(lv_r), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_r message!!\n");
    }

    free(buffer);
}

void client::rcv_leaving_r(uint8_t *buf, lv_r *pq)
{
    memcpy(pq, buf, sizeof(lv_r));
    fprintf(stdout, "client %s: Received leaving-r(0x%x)\n", name, pq->ni);
    fprintf(fptr, "received leaving-r(0x%x)\n", pq->ni);
    if (log_all)
        printRaw(fptr, buf, sizeof(lv_r));
}

void client::send_next_data_q()
{
    nd_q q; 
    q.type = NDT_Q; 
    q.di = pre_id; 
    q.id = 0xffffffff;
    
    int dest_port = pre_port;

    uint8_t *buffer = (uint8_t *) malloc(sizeof(nd_q));

    memcpy((void*)buffer, &q, sizeof(nd_q));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending next_data_q(0x%x, 0x%x) to dest on port %d\n",
        hashval, q.di, q.id, dest_port);

    fprintf(fptr, "next_data_q sent (0x%x 0x%x)\n", q.di, q.id);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(nd_q), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_r message!!\n");
    }

    free(buffer);
}

void client::rcv_next_data_q(uint8_t *buf, nd_q *pq) {
    memcpy(pq, buf, sizeof(nd_q));
    fprintf(stdout, "I'm 0x%x: Received next_data_q(0x%x, 0x%x)\n",
        hashval, pq->di, pq->id);

    fprintf(fptr, "received next_data_q(0x%x, 0x%x)\n", pq->di, pq->id);
    if (log_all)
        printRaw(fptr, buf, sizeof(nd_q));
}

int client::rcv_next_data_r(uint8_t *buf, nd_r *pdr) {
    
    nd_r *p = (nd_r *) buf;
    memcpy(pdr, p, sizeof(nd_r));

    if (pdr->di == hashval) {
        
        fprintf(stdout, "I'm 0x%x: Received next_data_r(0x%x, 0x%x, 0x%x, %d, %s)\n",
        hashval, pdr->di, pdr->qid, pdr->rid, pdr->SL, pdr->S);

        fprintf(fptr, "next-data-r received (0x%x 0x%x 0x%x %d %s)\n", 
                    pdr->di, pdr->qid, pdr->rid, pdr->SL, pdr->S);

        if (log_all)
            printRaw(fptr, buf, sizeof(nd_r));

        if (pdr->SL == 0)
            return 1;
        else {
            data_vec.push_back(data(pdr->S,pdr->rid));
            return 0;
        }
    } else {
        fprintf(stderr, "%s: next-data-r not intended for me!\n", name);
        die("Exiting");
    }

    // should never come here
    fprintf(stdout, "client %s: I should not be here!\n", name);
    return 0;
}

int client::send_next_data_r(nd_q * q) 
{
    // may have to revisit this logic
    nd_r r; r.type = NDT_R; r.di = suc_id;

    vector<data>::iterator piter;

    int dest_port = suc_port;

    r.qid = q->id;

    // qid, rid, SL, S
    if (data_vec.size()) {
        piter = data_vec.begin();

        r.rid = piter->dhash;
        strcpy(r.S, piter->ddata);
        r.SL = strlen(r.S);

        data_vec.erase(piter);
    } else {
        strcpy(r.S, "");
        r.SL = 0;
        r.rid = hashval - 1;
    } 

    uint8_t *buffer = (uint8_t *) malloc(sizeof(nd_r));

    memcpy((void*)buffer, &r, sizeof(nd_r));

    int sockfd, n;
    struct sockaddr_in myaddr;

    fprintf(stdout, "I'm 0x%x: Sending next-data-r(0x%x, 0x%x, 0x%x, %d, %s) to dest on port %d\n",
        hashval, r.di, r.qid, r.rid, r.SL, r.S, dest_port);

    fprintf(fptr, "next-data-r sent (0x%x 0x%x 0x%x %d %s)\n", r.di, r.qid, r.rid, r.SL, r.S);

    sockfd = get_listen_sock();

    memset(&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr= INADDR_ANY;//inet_addr("localhost");
    myaddr.sin_port =  htons(dest_port);

    n = sendto(sockfd, buffer, sizeof(nd_r), 0, (struct sockaddr *)&myaddr,sizeof(myaddr));

    if (n < 0)
    {
        free(buffer);
        die("failed to send udp: store_r message!!\n");
    }

    free(buffer);

    return (r.SL == 0);
}

void client::find_and_erase_port(uint32_t port)
{
    vector<uint32_t>::iterator piter = vec_ports.begin();
    vector<uint32_t>::iterator piter2 = vec_ids.begin();

    int flag = 0;
    while (piter != vec_ports.end()) {
        if (*piter == port) {
            fprintf(stdout, "%s: deleting port : %d\n", name, port);
            vec_ports.erase(piter);
            vec_ids.erase(piter2);

            flag = 1;
            break;
        }
        piter++;
        piter2++;
    }

    if (!flag) {
        fprintf(stderr, "%s: Cannot find the id, port in vec_ids, vec_ports to delete----------------\n", name);
    }
}
