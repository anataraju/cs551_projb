stage 5
# note, on the nonce line,
# replace dddd with the first four digits
# of your student id
nonce 1234
start_client alpha
start_client bravo
start_client charlie
start_client delta
start_client echo
start_client foxtrot
store alpha
store bravo
store charlie
store delta
store echo
store gamma
#store delta
store epsilon
#search beta
#search delta
store foxtrot
end_client delta
end_client bravo
end_client charlie
start_client tango
dump alpha
#dump bravo
#dump charlie
#dump delta
dump foxtrot
dump echo
dump tango